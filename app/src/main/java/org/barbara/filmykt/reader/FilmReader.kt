package org.barbara.filmykt.reader

import android.content.res.Resources
import org.barbara.filmykt.model.Film
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.*

class FilmReader {

    fun getFilms(res: Resources, watchedFilms: Boolean): MutableMap<String, Film> {

        val films = TreeMap<String, Film>()
        var reader: BufferedReader? = null

        try {
            val iStream = res.assets.open(FILE_NAME)
            reader = BufferedReader(InputStreamReader(iStream))

            reader.forEachLine {
                val row = it.split(SEPARATOR)
                if (row.size == 8) {
                    if (watchedFilms && convertToBool(row[7])) {
                        films.put(row[1], Film(row[0], row[1], row[2], row[3], row[4],
                                row[5], row[6], convertToBool(row[7])))
                    } else if (!watchedFilms && !convertToBool(row[7])) {
                        films.put(row[1], Film(row[0], row[1], row[2], row[3], row[4],
                                row[5], row[6], convertToBool(row[7])))
                    }
                }
            }

        } catch (ex: IOException) {
            ex.printStackTrace()
        } finally {
            try {
                if (reader != null) {
                    reader.close()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        return films
    }

    private fun convertToBool(isWatched: String): Boolean {
        if (YES.equals(isWatched, ignoreCase = true))
            return true
        return false
    }

    companion object {

        private val FILE_NAME = "filmy.csv"
        private val SEPARATOR = ";"
        private val YES = "y"
    }
}
