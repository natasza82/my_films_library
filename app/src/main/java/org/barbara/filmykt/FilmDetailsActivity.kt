package org.barbara.filmykt

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.TextView
import org.barbara.filmykt.model.Film

class FilmDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_detail)

        val film = intent.getSerializableExtra("film") as Film

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = film.name
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        val polishTitle = findViewById(R.id.polish_title) as TextView
        val director = findViewById(R.id.director) as TextView
        val year = findViewById(R.id.year) as TextView
        val genre = findViewById(R.id.genre) as TextView
        val country = findViewById(R.id.country) as TextView
        val actors = findViewById(R.id.actors) as TextView

        polishTitle.text = film.polishName
        director.text = film.director
        year.text = "(" + film.year + ")"
        genre.text = film.genre.replace(TO_REPLACE, REPLACE_BY)
        country.text = film.country.replace(TO_REPLACE, REPLACE_BY)
        actors.text = film.actors.replace(TO_REPLACE, REPLACE_BY)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private val TO_REPLACE = ","
        private val REPLACE_BY = ", "
    }

}
