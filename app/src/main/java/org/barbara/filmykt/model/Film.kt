package org.barbara.filmykt.model

import java.io.Serializable

class Film(var polishName: String, var name: String, var year: String, var director: String,
           var genre: String, var country: String, var actors: String, var isWatched: Boolean) : Serializable {

    var id: Int = 0

    constructor(id: Int, polishName: String, name: String, year: String, director: String,
                genre: String, country: String, actors: String, isWatched: Boolean) :
    this(polishName, name, year, director, genre, country, actors, isWatched) {
        this.id = id
    }
}
