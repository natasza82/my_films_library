package org.barbara.filmykt

import android.os.Bundle

import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import org.barbara.filmykt.db.DatabaseHandler
import org.barbara.filmykt.model.Film


class AddFilmFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        Utils.setFragmentTitle(activity, getString(R.string.title_section3))
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        if (inflater == null) {
            return null
        }
        var rootView = inflater.inflate(R.layout.fragment_add_film, container, false)
        val btn = rootView.findViewById(R.id.add_film_button) as Button

        btn.setOnClickListener {
            val title = rootView.findViewById(R.id.title) as EditText
            val polish_title = rootView.findViewById(R.id.polish_title) as EditText
            val director = rootView.findViewById(R.id.director) as EditText
            val year = rootView.findViewById(R.id.year) as EditText
            val genre = rootView.findViewById(R.id.genre) as EditText
            val country = rootView.findViewById(R.id.country) as EditText
            val actors = rootView.findViewById(R.id.actors) as EditText

            val db = DatabaseHandler(activity)
            val newFilm = Film(title.text.toString(), polish_title.text.toString(),
                    year.text.toString(), director.text.toString(), genre.text.toString(),
                    country.text.toString(), actors.text.toString(), false)

            db.addFilm(newFilm)
            Toast.makeText(activity, "Film added", Toast.LENGTH_SHORT).show()

            activity.supportFragmentManager.popBackStack()
        }
        return rootView
    }

}
