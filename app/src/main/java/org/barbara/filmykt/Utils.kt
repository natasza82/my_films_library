package org.barbara.filmykt

import android.app.Activity
import android.support.v7.app.AppCompatActivity

class Utils {

    companion object {
        fun setFragmentTitle(activity: Activity, title: String): Unit {
            val actionBar = (activity as AppCompatActivity).supportActionBar
            if (actionBar != null) {
                actionBar.title = title
                actionBar.setHomeButtonEnabled(true)
                actionBar.setDisplayHomeAsUpEnabled(true)
            }
        }
    }
}