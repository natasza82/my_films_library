package org.barbara.filmykt

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import org.barbara.filmykt.db.DatabaseHandler
import org.barbara.filmykt.model.Film

class EditFilmFragment : Fragment() {

    fun newInstance(filmTitle: String): EditFilmFragment {
        val fragment = EditFilmFragment()
        val bundle = Bundle()
        bundle.putString(EditFilmFragment.BUNDLE_FILM_TITLE, filmTitle)
        fragment.arguments = bundle
        return fragment
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        var bTitle = arguments.getString(BUNDLE_FILM_TITLE)
        Utils.setFragmentTitle(activity, bTitle)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        if (inflater == null) {
            return null
        }


        var bTitle = arguments.getString(BUNDLE_FILM_TITLE)
        val db = DatabaseHandler(activity)
        var films = db.watchedFilms as MutableMap<String, Film>
        films.putAll(db.notSeenFilms)
        var film = films[bTitle]

        var rootView = inflater.inflate(R.layout.fragment_add_film, container, false)
        val btn = rootView.findViewById(R.id.add_film_button) as Button
        val title = rootView.findViewById(R.id.title) as EditText
        val polishTitle = rootView.findViewById(R.id.polish_title) as EditText
        val director = rootView.findViewById(R.id.director) as EditText
        val year = rootView.findViewById(R.id.year) as EditText
        val genre = rootView.findViewById(R.id.genre) as EditText
        val country = rootView.findViewById(R.id.country) as EditText
        val actors = rootView.findViewById(R.id.actors) as EditText
        val button = rootView.findViewById(R.id.add_film_button) as Button


        title.setText(film?.name)
        polishTitle.setText(film?.polishName)
        director.setText(film?.director)
        year.setText(film?.year)
        genre.setText(film?.genre)
        country.setText(film?.country)
        actors.setText(film?.actors)
        button.text = resources.getText(R.string.edit_film)


        btn.setOnClickListener {
            if (film != null) {
                val updatedFilm = Film(film.id, polishTitle.text.toString(), title.text.toString(),
                        year.text.toString(), director.text.toString(), genre.text.toString(),
                        country.text.toString(), actors.text.toString(), film.isWatched)

                db.updateFilm(updatedFilm)
                Toast.makeText(activity, "Film edited", Toast.LENGTH_SHORT).show()
                activity.supportFragmentManager.popBackStack()
            }
        }
        return rootView
    }

    companion object {
        val BUNDLE_FILM_TITLE = "filmTitle"
    }

}
