package org.barbara.filmykt.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import org.barbara.filmykt.model.Film
import java.util.*

class DatabaseHandler(context: Context) : SQLiteOpenHelper(context, DatabaseHandler.DATABASE_NAME, null, DatabaseHandler.DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {

        val CREATE_FILMS_TABLE = CREATE_TABLE + TABLE_FILMS + L_BRACKET + KEY_ID + PKEY + COMA +
                KEY_PL_NAME + TEXT_TYPE + COMA + KEY_NAME + TEXT_TYPE + COMA + KEY_YEAR + TEXT_TYPE +
                COMA + KEY_DIRECTOR + TEXT_TYPE + COMA + KEY_GENRE + TEXT_TYPE + COMA + KEY_COUNTRY +
                TEXT_TYPE + COMA + KEY_ACTORS + TEXT_TYPE + COMA + KEY_WATCHED + TEXT_TYPE + R_BRACKET
        db.execSQL(CREATE_FILMS_TABLE)

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(DROP_TABLE + TABLE_FILMS)
        onCreate(db)
    }

    fun addFilm(film: Film) {
        val db = this.writableDatabase
        val values = ContentValues()

        values.put(KEY_PL_NAME, film.polishName)
        values.put(KEY_NAME, film.name)
        values.put(KEY_YEAR, film.year)
        values.put(KEY_DIRECTOR, film.director)
        values.put(KEY_GENRE, film.genre)
        values.put(KEY_COUNTRY, film.country)
        values.put(KEY_ACTORS, film.actors)
        values.put(KEY_WATCHED, film.isWatched)

        db.insert(TABLE_FILMS, null, values)
        db.close()
    }


    fun updateFilm(film: Film) {
        val db = this.writableDatabase
        val values = ContentValues()

        values.put(KEY_PL_NAME, film.polishName)
        values.put(KEY_NAME, film.name)
        values.put(KEY_YEAR, film.year)
        values.put(KEY_DIRECTOR, film.director)
        values.put(KEY_GENRE, film.genre)
        values.put(KEY_COUNTRY, film.country)
        values.put(KEY_ACTORS, film.actors)

        db.update(TABLE_FILMS, values, KEY_ID + " = ?", arrayOf(film.id.toString()))
        db.close()
    }

    fun markFilmWatched(film: Film) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_WATCHED, true)

        db.update(TABLE_FILMS, values, KEY_ID + " = ?", arrayOf(film.id.toString()))
        db.close()
    }

    fun deleteFilm(film: Film) {
        val db = this.writableDatabase
        db.delete(TABLE_FILMS, KEY_ID + " = ?",
                arrayOf(film.id.toString()))
        db.close()
    }

    val filmsCount: Int
        get() {
            val countQuery = "SELECT  * FROM " + TABLE_FILMS
            val db = this.readableDatabase
            val cursor = db.rawQuery(countQuery, null)
            cursor.close()

            return cursor.count
        }

    internal fun getFilm(id: Int): Film {
        val db = this.readableDatabase

        val cursor = db.query(TABLE_FILMS, arrayOf(KEY_ID, KEY_PL_NAME, KEY_NAME, KEY_YEAR,
                KEY_DIRECTOR, KEY_GENRE, KEY_ACTORS, KEY_COUNTRY, KEY_WATCHED), KEY_ID + "=?",
                arrayOf(id.toString()), null, null, null, null)

        cursor.moveToFirst()

        val film = Film(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5),
                cursor.getString(6), cursor.getString(7), java.lang.Boolean.parseBoolean(cursor.getString(8)))

        cursor.close()

        return film
    }


    fun getFilms(selectQuery: String): Map<String, Film> {
        val filmList : MutableMap<String,Film> = TreeMap()

        val db = this.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        if (cursor.moveToFirst()) {
            do {
                val film = Film(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5),
                        cursor.getString(6), cursor.getString(7), java.lang.Boolean.parseBoolean(cursor.getString(8)))

                filmList.put(cursor.getString(2), film)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return filmList
    }

    val allFilms: Map<String, Film>
        get() {
            val selectQuery = "SELECT * FROM " + TABLE_FILMS
            return getFilms(selectQuery)
        }

    val watchedFilms: Map<String, Film>
        get() {
            val selectQuery = "SELECT * FROM $TABLE_FILMS WHERE $KEY_WATCHED = '1'"
            return getFilms(selectQuery)
        }

    val notSeenFilms: Map<String, Film>
        get() {
            val selectQuery = "SELECT  * FROM $TABLE_FILMS WHERE $KEY_WATCHED = '0'"
            return getFilms(selectQuery)
        }

    companion object {

        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "filmsManager"
        private val L_BRACKET = "("
        private val R_BRACKET = ")"
        private val TEXT_TYPE = " TEXT"
        private val COMA = ","
        private val PKEY = " INTEGER PRIMARY KEY "
        private val CREATE_TABLE = "CREATE TABLE "
        private val DROP_TABLE = "DROP TABLE IF EXISTS "

        private val TABLE_FILMS = "films"

        private val KEY_ID = "id"
        private val KEY_NAME = "name"
        private val KEY_PL_NAME = "polish_name"
        private val KEY_YEAR = "year"
        private val KEY_DIRECTOR = "director"
        private val KEY_GENRE = "genre"
        private val KEY_ACTORS = "actors"
        private val KEY_COUNTRY = "country"
        private val KEY_WATCHED = "watched"
    }
}

