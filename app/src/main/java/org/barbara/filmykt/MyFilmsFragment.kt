package org.barbara.filmykt

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.text.TextUtils
import android.view.*
import android.widget.*
import org.barbara.filmykt.db.DatabaseHandler
import org.barbara.filmykt.model.Film
import org.barbara.filmykt.reader.FilmReader
import java.util.*

class MyFilmsFragment : Fragment(), SearchView.OnQueryTextListener {

    private var mSearchView: SearchView? = null
    private var list: ListView? = null
    internal var adapter: ArrayAdapter<String>? = null
    internal var watchedFilms: Boolean = false
    private var filmsList: MutableList<String> = ArrayList()
    private var films: Map<String, Film>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        watchedFilms = arguments.getBoolean(BUNDLE_IS_WATCHED)
        if (watchedFilms) {
            Utils.setFragmentTitle(activity, getString(R.string.title_section1))
        } else {
            Utils.setFragmentTitle(activity, getString(R.string.title_section2))
        }

        val rootView = inflater!!.inflate(R.layout.fragment_my_films, container, false)

        val fab = rootView.findViewById(R.id.fab) as FloatingActionButton
        fab.setOnClickListener {
            val fragmentManager: FragmentManager = activity.supportFragmentManager;
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, AddFilmFragment()).commit();
        }

        initializeListView(rootView)

        mSearchView = rootView.findViewById(R.id.search_view) as SearchView

        mSearchView?.setIconifiedByDefault(false)
        mSearchView?.setOnQueryTextListener(this)
        mSearchView?.isSubmitButtonEnabled = false
        mSearchView?.queryHint = getString(R.string.search_by_title)
        activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return rootView
    }

    private fun initializeListView(rootView: View?) {

        val db = DatabaseHandler(context)
        if (watchedFilms) {
            films = db.watchedFilms
        } else {
            films = db.notSeenFilms
        }
        filmsList.clear()

        filmsList.addAll(films!!.keys)

        adapter = ArrayAdapter(activity, R.layout.list_item, filmsList)

        if (rootView != null) {
            list = rootView.findViewById(R.id.film_list) as ListView
            list?.adapter = adapter
            list?.isTextFilterEnabled = true
            registerForContextMenu(list)

            list?.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
                val title = parent.getItemAtPosition(position).toString()
                val clickedFilm = (films as Map<String, Film>)[title]

                val intent = Intent(activity, FilmDetailsActivity::class.java)
                intent.putExtra("film", clickedFilm)
                startActivity(intent)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item!!.itemId

        if (id == R.id.action_synchronize) {
            synchronizeFilms()
            initializeListView(view)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun synchronizeFilms() {
        val reader = FilmReader()
        var filmsSynch = reader.getFilms(resources, true)
        filmsSynch.putAll(reader.getFilms(resources, false))
        val db = DatabaseHandler(context)
        for (film in filmsSynch.values) {
            db.addFilm(film)
        }
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (TextUtils.isEmpty(newText)) {
            list?.clearTextFilter()
        } else {
            list?.setFilterText(newText)
        }
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View,
                                     menuInfo: ContextMenu.ContextMenuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = activity.menuInflater
        inflater.inflate(R.menu.context_menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        val info = item!!.menuInfo as AdapterView.AdapterContextMenuInfo
        when (item.itemId) {
            R.id.edit -> {
                editFilm((info.targetView as TextView).text.toString())
                return true
            }
            R.id.mark_watched -> {
                markFilmWatched(info.id.toInt())
                return true
            }
            R.id.delete -> {
                deleteFilm(info.id.toInt())
                return true
            }
            else -> return super.onContextItemSelected(item)
        }
    }

    private fun markFilmWatched(position: Int) {
        val db = DatabaseHandler(context)

        val title = filmsList[position.toInt()]

        val film = films!![title]
        filmsList.removeAt(position)
        adapter = ArrayAdapter(activity,
                R.layout.list_item, filmsList)
        list?.adapter = adapter

        (adapter as ArrayAdapter<String>).notifyDataSetChanged()

        if (film != null) {
            db.markFilmWatched(film)
        }
    }

    private fun editFilm(title: String) {

        val fragment = EditFilmFragment().newInstance(title);
        val fragmentManager: FragmentManager = activity.supportFragmentManager;
        fragmentManager.beginTransaction()
                .replace(R.id.frame_container, fragment).addToBackStack(null).commit();

    }

    private fun deleteFilm(position: Int) {
        val db = DatabaseHandler(context)

        val title = filmsList[position.toInt()]

        val film = films!![title]
        filmsList.removeAt(position)
        adapter = ArrayAdapter(activity,
                R.layout.list_item, filmsList)
        list?.adapter = adapter

        (adapter as ArrayAdapter<String>).notifyDataSetChanged()

        if (film != null) {
            db.deleteFilm(film)
        }
    }

    companion object {

        val BUNDLE_IS_WATCHED = "watchedFilms"
    }
}
